Rails.application.routes.draw do
  resources :products do
    collection do
      post 'pay/:id'      => 'products#pay', as: 'pay'
      get  'done'         => 'products#done', as: 'done'
     end
  end
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', registrations: 'users/registrations' }
  get 'static_pages/index'
  root 'static_pages#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
