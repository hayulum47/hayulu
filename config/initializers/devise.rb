
Devise.setup do |config|
  config.mailer_sender = 'please-change-me-at-config-initializers-devise@example.com'
  require 'devise/orm/active_record'
  config.case_insensitive_keys = [:email]
  config.strip_whitespace_keys = [:email]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 11
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 6..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
  config.scoped_views = true
  
  config.omniauth :facebook,   '2554564087972045', '0440edd94b76749ea3ab3e6e7ffa25ff'
  config.omniauth :instagram,  '992553541123211',  '3c1f9c7e48fbf75c362cd5e9048dca1d'
end
