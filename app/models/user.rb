class User < ApplicationRecord  
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :omniauthable
  
  attr_writer :last_name, :first_name
  
  before_validation :set_name
  
  
  def last_name
    @last_name || self.name.split(" ").first if self.name.present?
  end
  
  def first_name
    @first_name || self.name.split(" ").last if self.name.present?
  end
  
  def set_name
    self.name = [@last_name, @first_name].join(" ")
  end
         
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |u|
      u.email = auth.info.email
      u.password = Devise.friendly_token[0,20]
      u.name = auth.info.name   # assuming the user model has a name
      u.image = auth.info.image # assuming the user model has an image
    end
    
    # ユーザーはSNSで登録情報を変更するかもしれないので、毎回データベースの情報も更新する
    self.find_or_create_by(provider: provider, uid:uid) do |u|
      u.username   = name
      u.image_path = image
    end
  end
end
